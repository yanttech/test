<?php

declare(strict_types=1);

namespace Yant\SerpBenchmark\Serp;

use Yant\SerpBenchmark\Exception\SerpException;

/**
 * Serp service
 */
interface SerpServiceInterface
{
    /**
     * Get Serp URLs list
     *
     * @param string $query
     * @return array
     * @throws SerpException
     */
    public function search(string $query): array;
}
