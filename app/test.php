<?php

use Yant\SerpBenchmark\Bench\Benchmark;
use Yant\SerpBenchmark\Cache\FakeCache;
use Yant\SerpBenchmark\Exception\InvalidArgumentException as BenchInvalidArgumentException;
use Yant\SerpBenchmark\Serp\SerpService;
use function Co\run;

require_once "vendor/autoload.php";

$config = require_once "config.php";

$search = $argv[1] ?? throw new BenchInvalidArgumentException('No search string provided');

// cache
$cache = new FakeCache();

// get serp
$serpEntries = (new SerpService)->search($search);

// recommendations
$response = new stdClass();
$response->recommendations = [];

error_reporting(0);
ini_set('display_errors', 0);
ob_start();

run(function () use ($config, $search, $cache, $serpEntries, $response) {

    foreach ($serpEntries as $serpEntry) {
        go(function () use ($serpEntry, $cache, $config, $response) {
            $serpEntry->recommendedConnections = $cached = $cache->get($serpEntry->Host) ??
                (new Benchmark)
                    ->startWith($config['connections']['start_connections_count'] ?? 10)
                    ->max($config['connections']['max_connections_count'] ?? 10)
                    ->timeout($config['connections']['timeout'] ?? 5)
                    ->pause($config['connections']['pause'] ?? 0)
                    ->marginOfSafety($config['connections']['margin_of_safety'] ?? .1)
                    ->url($serpEntry->Url)
                    ->load();

            if (null === $cached) {
                $cache->put($serpEntry->Host, $serpEntry->recommendedConnections);
            }

            $response->recommendations[$serpEntry->Host] = $serpEntry->recommendedConnections;
        });
    }
});
ob_end_clean();

echo json_encode($response->recommendations);
