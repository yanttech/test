<?php

declare(strict_types=1);

namespace Yant\SerpBenchmark\Bench;

/**
 * Run load
 */
interface LoadInterface
{
    /**
     * Load
     *
     * @param string $url
     * @return object
     */
    public function load(string $url): object;

    /**
     * Connections count
     *
     * @param int $count
     * @return static
     */
    public function count(int $count): static;

    /**
     * Set timeout
     *
     * @param int $timeout
     * @return static
     */
    public function timeout(int $timeout): static;
}
