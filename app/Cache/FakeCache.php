<?php

declare(strict_types=1);

namespace Yant\SerpBenchmark\Cache;

/**
 * Fake cache for testing purposes
 */
class FakeCache implements CacheInterface
{
    /**
     * Get value from cache by key, null if not found
     *
     * @param string $key
     * @return mixed
     */
    public function get(string $key): mixed
    {
        return null;
    }

    /**
     * Set value
     *
     * @param string $key
     * @param mixed $value
     */
    public function put(string $key, mixed $value): void
    {
    }
}
