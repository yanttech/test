<?php

return [
    // swoole configuration
    'swoole' => [],
    // benchmark configuration
    'connections' => [
        'start_connections_count' => 3,
        'max_connections_count' => 3,
        'margin_of_safety' => 0.0,
        'timeout' => 3,
        'pause' => 0,
    ],
];
