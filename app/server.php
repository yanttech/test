<?php

declare(strict_types=1);

namespace Yant\SerpBenchmark;

use Swoole\Http\Server;
use Swoole\Http\Request;
use Swoole\Http\Response;
use Yant\SerpBenchmark\Http\Router;
use Yant\SerpBenchmark\Http\ServerResponse;

require_once "vendor/autoload.php";
$config = require_once "config.php";

// router configuration

$router = new Router();

$router->addEndpoint('get', 'sites', function (Request $request, Response $response) {

    $search = $request->get['search'] ?? false;

    // no search string defined
    if (false === $search) {
        ServerResponse::failure($response, 500, 'Invalid request - no search string defined');
        return;
    }

    $search = addslashes($search);

    exec("php test.php $search", $output, $code);

    if (0 !== $code) {
        ServerResponse::failure($response, 500, 'Something gone wrong');
        return;
    }

    $recommendations = json_decode($output[0]);

    ServerResponse::success($response, $recommendations);
});


$router->addEndpoint('get', 'php', function(Request $request, Response $response) {
    ob_start();
    phpinfo();
    $result = ob_get_clean();
    $response->end($result);
});

$router->callbackOnFailure(function (Response $response, string $message) {
    ServerResponse::failure($response, 500, $message);
});

$router->callbackOnNotFound(function (Response $response) {
    ServerResponse::failure($response, 404, 'Method not found');
});

// start server

$server = new Server("0.0.0.0", 9501);

$server->on('Request', function (Request $request, Response $response) use ($router, $server) {
    $router->execute($request, $response);
});

$server->start();
