<?php

declare(strict_types=1);

namespace Yant\SerpBenchmark\Bench;

use Swoole\Coroutine;
use Yant\SerpBenchmark\Exception\LoadException;

/**
 * Benchmark
 */
class Benchmark implements BenchmarkInterface
{
    protected ?string $url = null;
    protected int $maxConnections = 500;
    protected int $startWith = 250;
    protected float $marginOfSafety = .1;
    protected int $pause = 0;
    protected int $timeout = 5;

    /**
     * URL for testing
     *
     * @param string $url
     * @return static
     */
    public function url(string $url): static
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Maximum connections
     *
     * @param int $maxConnections
     * @return static
     */
    public function max(int $maxConnections): static
    {
        $this->maxConnections = $maxConnections;

        return $this;
    }

    /**
     * Set initial connections count
     *
     * @param int $connectionsCount
     * @return static
     */
    public function startWith(int $connectionsCount): static
    {
        $this->startWith = $connectionsCount;

        return $this;
    }

    /**
     * Set connections timeout
     *
     * @param int $seconds
     * @return static
     */
    public function timeout(int $seconds): static
    {
        $this->timeout = $seconds;

        return $this;
    }

    /**
     * Pause between loads
     *
     * @param int $seconds
     * @return static
     */
    public function pause(int $seconds): static
    {
        $this->pause = $seconds;

        return $this;
    }

    /**
     * Margin of safety
     *
     * @param float $margin
     * @return static
     */
    public function marginOfSafety(float $margin): static
    {
        $this->marginOfSafety = $margin;

        return $this;
    }

    /**
     * Run benchmark
     *
     * @return int recommended connections amount
     * @throws LoadException
     */
    public function load(): int
    {
        if (null === $this->url) {
            throw new LoadException('URL is not specified');
        }

        $connectionsCount = $this->startWith;
        $maxConnections = $this->maxConnections;
        $pause = $this->pause;
        $strategy = 'increase';

        $url = $this->url;
        $timeout = $this->timeout;

        while (1) {

            $results = (new CoroutineLoad())->count($connectionsCount)->timeout($timeout)->load($url);

            // some requests responded with error - we should decrease connections amount
            if ($results->total !== $results->succeded) {
                $connectionsCount = max(0, (int)floor($results->succeded - $results->succeded * $this->marginOfSafety));
                $strategy = 'decrease';

                // no good responses
                if ($connectionsCount === 0) {
                    break;
                }

                if ($pause !== 0) {
                    Coroutine::sleep($pause);
                }
                continue;
            }

            // increasing connections count strategy - considered server can do more connections
            if ('increase' === $strategy) {

                if ($connectionsCount === $maxConnections) {
                    break;
                }

                $connectionsCount = min($maxConnections, $connectionsCount * 2);

                if ($pause !== 0) {
                    Coroutine::sleep($pause);
                }
                continue;
            }

            // decreased connections amount, it worked, there is no need for further tests
            if ('decrease' === $strategy) {
                break;
            }
        }

        return $connectionsCount;
    }
}
