<?php

declare(strict_types=1);

namespace Yant\SerpBenchmark\Http;

use Swoole\Http\Request;
use Swoole\Http\Response;
use Yant\SerpBenchmark\Exception\HttpException;

/**
 * Simple HTTP router for Swoole
 */
interface RouterInterface
{
    /**
     * Add an endpoint to the router
     *
     * @param string $method
     * @param string $path
     * @param callable $callable
     * @return static
     * @throws HttpException
     */
    public function addEndpoint(string $method, string $path, callable $callable): static;

    /**
     * Remove endpoint from the router
     *
     * @param string $method
     * @param string $path
     * @return static
     * @throws HttpException
     */
    public function removeEndpoint(string $method, string $path): static;

    /**
     * Set failure callback
     * Callable must accept Swoole\Http\Response and the message string as a parameters
     *
     * @param callable|null $callable
     * @return static
     */
    public function callbackOnFailure(callable|null $callable): static;

    /**
     * Set 404 callback
     * Callable must accept Swoole\Http\Response as a parameter
     *
     * @param callable|null $callable
     * @return static
     */
    public function callbackOnNotFound(callable|null $callable): static;

    /**
     * Handle the route
     *
     * @param Request $request
     * @param Response $response
     * @return static
     */
    public function execute(Request $request, Response $response): static;
}
