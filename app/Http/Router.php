<?php

declare(strict_types=1);

namespace Yant\SerpBenchmark\Http;

use Closure;
use Swoole\Http\Request;
use Swoole\Http\Response;
use Throwable;
use Yant\SerpBenchmark\Exception\HttpException;
use Yant\SerpBenchmark\Exception\InvalidArgumentException;

/**
 * Simple HTTP router for Swoole
 */
class Router implements RouterInterface
{
    public const ALLOWED_METHODS = ['GET', 'POST', 'OPTIONS'];

    protected array $routes = [];

    protected Closure|string|null $onFailure = null;
    protected Closure|string|null $onNotFound = null;

    /**
     * Add an endpoint to the router
     *
     * @param string $method
     * @param string $path
     * @param callable $callable
     * @return static
     * @throws InvalidArgumentException
     */
    public function addEndpoint(string $method, string $path, callable $callable): static
    {
        try {
            $this->validateMethod($method = $this->normalizeMethod($method));
        } catch (HttpException $exception) {
            throw new InvalidArgumentException($exception->getMessage());
        }

        $path = $this->normalizePath($path);

        if (!array_key_exists($method, $this->routes)) {
            $this->routes[$method] = [];
        }

        $this->routes[$method][] = [
            'path' => $path,
            'callable' => $callable,
        ];

        return $this;
    }

    /**
     * Remove endpoint from the router
     *
     * @param string $method
     * @param string $path
     * @return static
     * @throws HttpException
     */
    public function removeEndpoint(string $method, string $path): static
    {
        $this->validateMethod($method = $this->normalizeMethod($method));
        $path = $this->normalizePath($path);

        if (!array_key_exists($method, $this->routes)) {
            return $this;
        }

        $this->routes[$method] = array_filter(
            array_map(
                fn($entry) => $entry['path'] !== $path ? $entry : null, $this->routes[$method]
            )
        );

        return $this;
    }

    /**
     * Handle the route
     *
     * @param Request $request
     * @param Response $response
     * @return static
     */
    public function execute(Request $request, Response $response): static
    {
        try {
            $method = $request->server['request_method'];
            $path = $request->server['request_uri'];

            $this->validateMethod($method = $this->normalizeMethod($method));
            $path = $this->normalizePath($path);

            $callable = $this->getRouteCallable($method, $path);

            // endpoint exists
            if (false !== $callable) {
                $callable($request, $response);

                return $this;
            }

            // 404 behaviour
            if (null !== $this->onNotFound) {
                ($this->onNotFound)($response);
            }

        } catch (Throwable $exception) {
            if (null !== $this->onFailure) {
                ($this->onFailure)($response, $exception->getMessage());
            }
        }

        return $this;
    }

    /**
     * Get route callable
     *
     * @param string $method
     * @param string $path
     * @return callable|false
     */
    protected function getRouteCallable(string $method, string $path): callable|false
    {
        $result = false;

        foreach ($this->routes[$method] ?? [] as $entry) {
            if ($entry['path'] === $path) {
                $result = $entry['callable'];
                break;
            }
        }

        return $result;
    }

    /**
     * Set failure callback
     * Callable must accept Swoole\Http\Response and the message string as a parameters
     *
     * @param callable|null $callable
     * @return static
     */
    public function callbackOnFailure(callable|null $callable): static
    {
        $this->onFailure = $callable;

        return $this;
    }

    /**
     * Set 404 callback
     * Callable must accept Swoole\Http\Response as a parameter
     *
     * @param callable|null $callable
     * @return static
     */
    public function callbackOnNotFound(callable|null $callable): static
    {
        $this->onNotFound = $callable;

        return $this;
    }

    /**
     * Normalize method string
     *
     * @param string $method
     * @return string
     */
    protected function normalizeMethod(string $method): string
    {
        return strtoupper(trim($method));
    }

    /**
     * Validate the HTTP request method
     *
     * @param string $method
     * @return static
     * @throws HttpException
     */
    protected function validateMethod(string $method): static
    {
        if (!in_array($method, static::ALLOWED_METHODS)) {
            throw new HttpException('Invalid HTTP method');
        }

        return $this;
    }

    /**
     * Normalize path - trim spaces and /
     *
     * @param string $path
     * @return string
     */
    protected function normalizePath(string $path): string
    {
        return trim($path, " \t\n\r\0\x0B/");
    }
}
