<?php

declare(strict_types=1);

namespace Yant\SerpBenchmark\Exception;

use Exception;

/**
 * Benchmark exception
 */
class LoadException extends Exception implements BenchmarkException
{
}
