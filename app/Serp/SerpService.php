<?php

declare(strict_types=1);

namespace Yant\SerpBenchmark\Serp;

use JsonException;
use Yant\SerpBenchmark\Exception\SerpException;

/**
 * Serp
 */
class SerpService implements SerpServiceInterface
{
    protected const SERVICE_COMMAND = 'cd /var/serp && go run serp.go ';

    /**
     * Get Serp URLs list
     *
     * @param string $query
     * @return array
     * @throws SerpException
     */
    public function search(string $query): array
    {
        $query = static::SERVICE_COMMAND . '"' . addslashes($query) . '"';

        try {
            $result = json_decode(exec($query), flags: JSON_INVALID_UTF8_IGNORE | JSON_THROW_ON_ERROR);
        } catch (JsonException $e) {
            throw new SerpException($e->getMessage());
        }

        $urls = $result->Items ?? [];

        if (empty($urls)) {
            throw new SerpException('No results');
        }

        return $urls;
    }
}
