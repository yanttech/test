<?php

declare(strict_types=1);

namespace Yant\SerpBenchmark\Bench;

use stdClass;
use Yurun\Util\HttpRequest;
use Yurun\Util\YurunHttp\Co\Batch;
use Yurun\Util\YurunHttp\Http\Psr7\Response;

/**
 * Coroutine load
 */
class CoroutineLoad implements LoadInterface
{
    protected int $connectionsCount = 500;
    protected int $timeout = 3;

    /**
     * Load
     *
     * @param string $url
     * @return object
     */
    public function load(string $url): object
    {
        $requests = [];
        for ($i = 0; $i < $this->connectionsCount; $i++) {
            $requests[] = (new HttpRequest)->url($url)->headers([
                'User-Agent' => 'Chrome/49.0.2587.3',
                'Accept' => 'text/html,application/xhtml+xml,application/xml',
                'Accept-Encoding' => 'gzip',
            ]);
        }

        $responses = Batch::run($requests, $this->timeout);

        $results = new stdClass();
        $results->total = $results->succeded = 0;

        foreach ($responses as $response) {
            $results->total++;
            $succeeded = (int)($response instanceof Response && in_array($response->getStatusCode() ?? 0, [200, 301, 302]));
            $results->succeded += $succeeded;
        }

        return $results;
    }

    /**
     * Connections count
     *
     * @param int $count
     * @return static
     */
    public function count(int $count): static
    {
        $this->connectionsCount = $count;

        return $this;
    }

    /**
     * Set timeout
     *
     * @param int $timeout
     * @return static
     */
    public function timeout(int $timeout): static
    {
        $this->timeout = $timeout;

        return $this;
    }
}
