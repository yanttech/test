<?php

declare(strict_types=1);

namespace Yant\SerpBenchmark\Exception;

use Throwable;

/**
 * Common service exception
 */
interface BenchmarkException extends Throwable
{
}
