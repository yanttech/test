<?php

declare(strict_types=1);

namespace Yant\SerpBenchmark\Exception;

use Exception;

/**
 * Request exception
 */
class HttpException extends Exception implements BenchmarkException
{
}
