<?php

declare(strict_types=1);

namespace Yant\SerpBenchmark\Exception;

use Exception;

/**
 * Serp service exception
 */
class SerpException extends Exception implements BenchmarkException
{
}
