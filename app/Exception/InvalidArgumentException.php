<?php

declare(strict_types=1);

namespace Yant\SerpBenchmark\Exception;

/**
 * Invalid argument exception
 */
class InvalidArgumentException extends \InvalidArgumentException implements BenchmarkException
{
}
