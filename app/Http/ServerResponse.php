<?php

declare(strict_types=1);

namespace Yant\SerpBenchmark\Http;

use Swoole\Http\Response;

/**
 * JSON server response helper functions
 */
class ServerResponse
{
    /**
     * Return failure response
     *
     * @param Response $response
     * @param int $status
     * @param string $message
     */
    public static function failure(Response $response, int $status, string $message): void
    {
        $response->status($status);

        $response->end(json_encode([
            'status' => 'error',
            'message' => $message,
        ]));
    }

    /**
     * Return success response
     *
     * @param Response $response
     * @param array $data
     */
    public static function success(Response $response, array $data): void
    {
        $response->end(json_encode([
            'status' => 'ok',
            'response' => $data
        ]));
    }
}
