<?php

declare(strict_types=1);

namespace Yant\SerpBenchmark\Bench;

use Yant\SerpBenchmark\Exception\LoadException;

/**
 * Benchmark
 */
interface BenchmarkInterface
{
    /**
     * URL for testing
     *
     * @param string $url
     * @return static
     */
    public function url(string $url): static;

    /**
     * Maximum connections
     *
     * @param int $maxConnections
     * @return static
     */
    public function max(int $maxConnections): static;

    /**
     * Set initial connections count
     *
     * @param int $connectionsCount
     * @return static
     */
    public function startWith(int $connectionsCount): static;

    /**
     * Set connections timeout
     *
     * @param int $seconds
     * @return static
     */
    public function timeout(int $seconds): static;

    /**
     * Pause between loads
     *
     * @param int $seconds
     * @return static
     */
    public function pause(int $seconds): static;

    /**
     * Margin of safety
     *
     * @param float $margin
     * @return static
     */
    public function marginOfSafety(float $margin): static;

    /**
     * Run benchmark
     *
     * @return int recommended connections amount
     * @throws LoadException
     */
    public function load(): int;
}
