FROM phpswoole/swoole:4.7-php8.0

RUN apt-get update \
  && apt-get install -y wget \
  && apt-get install -y apache2-utils \
  && rm -rf /var/lib/apt/lists/*

RUN cd /var && wget https://golang.org/dl/go1.17.1.linux-amd64.tar.gz
#COPY ./go1.17.1.linux-amd64.tar.gz /var/go1.17.1.linux-amd64.tar.gz
RUN tar -C /usr/local -xzf /var/go1.17.1.linux-amd64.tar.gz

ENV PATH=$PATH:/usr/local/go/bin

COPY ./serp /var/serp
COPY ./app /var/www

RUN cd /var/serp && go run serp.go
RUN cd /var/www && composer install

COPY ./config/php.ini-development /usr/local/etc/php/php.ini

WORKDIR /var/www